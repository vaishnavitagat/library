"""
This module contains book details and category of book
"""
import enum
from sqlalchemy import Column, ForeignKey
from sqlalchemy import String, Integer, Float, Enum
from sqlalchemy.orm import relationship
from models.db_enums import BookCategoryEnum

from models import EntityBase, ModifiedMixin, CreatedMixin


class Book(EntityBase, ModifiedMixin, CreatedMixin):
    """
    Element represents a user.
    """
    __tablename__ = 'book'
    __table_args__ = {'schema': 'application'}

    all_book_attributes = ['book_name', 'id', 'category', 'isbn', 'price', 'author', 'publisher']

    # Scalar properties
    book_name = Column(String, nullable=False)
    category = Column(Enum(BookCategoryEnum))
    isbn = Column(Integer, nullable=False)
    price = Column(Float, nullable=False)
    author = Column(String, nullable=False)
    publisher = Column(String, nullable=False)

    #relational properties
    stock = relationship('BookStock', backref='book', uselist=False)


class BookStock(EntityBase, ModifiedMixin, CreatedMixin):
    """
    Element represents a stock
    """
    __tablename__ = 'book_stock'
    __table_args__ = {'schema': 'application', 'extend_existing': True}

    # Scalar properties
    book_id = Column(Integer, ForeignKey(Book.id, onupdate="cascade", ondelete="cascade"))
    total_copies = Column(Integer, nullable=False)
    available_copies = Column(Integer, nullable=False)
    all_stock_attributes = ['book_id', 'total_copies', 'available_copies']

    #relational properties
    book_details = relationship('Book', foreign_keys=book_id)