"""
this module is related to the issued books and details of issued book
"""

from sqlalchemy import Column, ForeignKey
from sqlalchemy import Integer, Date, Enum, DECIMAL, Boolean
from sqlalchemy.orm import relationship
from models.book import Book
from models.reader import Reader
from models import EntityBase, ModifiedMixin, CreatedMixin
from models.db_enums import IssuedBookStatusEnum


class IssuedBooks(EntityBase, ModifiedMixin, CreatedMixin):
    """
    Element represents a user.
    """
    __tablename__ = 'issued_books'
    __table_args__ = {'schema': 'application'}

    # Scalar properties
    reader_id = Column(Integer, ForeignKey(Reader.id, onupdate="cascade", ondelete="cascade"))
    book_id = Column(Integer, ForeignKey(Book.id, onupdate="cascade", ondelete="cascade"))
    issued_date = Column(Date, nullable=False)
    due_date = Column(Date, nullable=False)
    status = Column(Enum(IssuedBookStatusEnum), nullable=False)
    delayed_days = Column(Integer)
    fine_amt = Column(DECIMAL)
    is_returned = Column(Boolean, nullable=False)


    #relational properties
    reader_info = relationship('Reader', backref='issued_books')
    book_info = relationship('Book', backref='issued_books')
