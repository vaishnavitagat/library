"""
Element represents the librarian and related functionality
"""
from sqlalchemy import Column, String
from models import EntityBase, ModifiedMixin, CreatedMixin


class Librarian(EntityBase, ModifiedMixin, CreatedMixin):
    """
    Element represents a librarian
    """
    __tablename__ = 'librarian'
    __table_args__ = {'schema': 'application'}

    # Scalar properties
    librarian_name = Column(String, nullable=False)
