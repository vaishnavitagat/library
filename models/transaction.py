"""
Module represents the creation of transaction
"""
import enum
from sqlalchemy import Column, ForeignKey
from sqlalchemy import Integer, Enum
from sqlalchemy.orm import relationship
from models import EntityBase, ModifiedMixin, CreatedMixin
from models.account import Account
from models.reader import Reader
from models.db_enums import TransactionType, TransactionComment


class Transaction(EntityBase, ModifiedMixin, CreatedMixin):
    """
    Element represents a transaction.
    """
    __tablename__ = 'transaction'
    __table_args__ = {'schema': 'application'}

    # Scalar properties
    account_no = Column(Integer, ForeignKey(Account.account_no, onupdate="cascade", ondelete="cascade"))
    reader_id = Column(Integer, ForeignKey(Reader.id, onupdate="cascade", ondelete="cascade"))
    amount = Column(Integer)
    transaction_type = Column(Enum(TransactionType))
    opening_balance = Column(Integer, nullable=False)
    closing_balance = Column(Integer, nullable=False)
    comment = Column(Enum(TransactionComment))

    #relational properties
    reader_info = relationship('Reader', backref='transaction')
