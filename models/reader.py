"""
Element represents the creation of reader
"""

from sqlalchemy import String, Integer, Enum, Date, Column, ForeignKey
from sqlalchemy_utils import EmailType
from sqlalchemy.orm import relationship
from models import EntityBase, CreatedMixin, ModifiedMixin
from models.readertype import ReaderType
from models.db_enums import PeriodOfMembership


class Reader(EntityBase, ModifiedMixin, CreatedMixin):
    """
    Element represents a reader
    """
    __tablename__ = 'reader'
    __table_args__ = {'schema': 'application', 'extend_existing': True}

    # Scalar properties
    name = Column(String, nullable=False)
    reader_type_id = Column(Integer, ForeignKey(ReaderType.id,
                                                onupdate="cascade", ondelete="cascade"), nullable=False)
    phone_no = Column(String(10), nullable=False)
    email_id = Column(EmailType, nullable=False)
    expiry_date = Column(Date, nullable=False)
    password_hash = Column(String, nullable=False)
    hint = Column(String)
    last_login = Column(Date)
    period_of_membership = Column(Enum(PeriodOfMembership))
    all_reader_attributes = ['name', 'reader_type_id', 'phone_no', 'email_id', 'expiry_date',
                             'last_login', 'period_of_membership']

    #relational properties
    reader_type_details = relationship('ReaderType', backref='reader')

    def get_reader_as_dict(self):
        """
        method to get reader as a dictionary
        """
        reader_dict = {}
        reader_dict['Name'] = self.name
        reader_dict['Reader Type'] = self.reader_type_id
        reader_dict['Phone no'] = self.phone_no
        reader_dict['Email ID'] = self.email_id
        reader_dict['Expiry Date'] = self.expiry_date
        reader_dict['Hint'] = self.hint
        reader_dict['Last login'] = self.last_login
        reader_dict['Period of Membership'] = self.period_of_membership.value
        reader_dict['Created time'] = self.created
        reader_dict['Modified time'] = self.modified
        return reader_dict
