"""
this modules contains password history and details
"""

from sqlalchemy import Column, ForeignKey
from sqlalchemy import String, Integer
from sqlalchemy.types import DateTime
from sqlalchemy.orm import relationship
from models import EntityBase, ModifiedMixin, CreatedMixin
from models.reader import Reader

class PasswordHistory(EntityBase, ModifiedMixin, CreatedMixin):
    """
    Element represents  password history
    """
    __tablename__ = 'password_history'
    __table_args__ = {'schema': 'application'}

    # Scalar properties
    reader_id = Column(Integer, ForeignKey(Reader.id, onupdate="cascade", ondelete="cascade"))
    old_passwd = Column(String)
    reset_passwd_token = Column(String)
    expires_on = Column(DateTime, nullable=False)

    #relational properties
    reader_info = relationship('Reader', backref='password_history')
