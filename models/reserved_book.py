"""
this is a model which contains all reserved book details
"""
from sqlalchemy import Date
from sqlalchemy import ForeignKey, Column, Integer
from models.book import Book
from models.reader import Reader
from models import EntityBase, ModifiedMixin, CreatedMixin

class ReservedBook(EntityBase, ModifiedMixin, CreatedMixin):
    """
    Element represents a reserved book.
    """
    __tablename__ = 'reserved_book'
    __table_args__ = {'schema': 'application'}

    # Scalar properties
    book_id = Column(Integer, ForeignKey(Book.id))
    reader_id = Column(Integer, ForeignKey(Reader.id))
    reserved_date = Column(Date, nullable=False)
    pickup_date = Column(Date)
