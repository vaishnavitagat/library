"""
This module represents prebook
"""

from sqlalchemy import Column, ForeignKey
from sqlalchemy import Integer, Date
from sqlalchemy.orm import relationship
from models import EntityBase, ModifiedMixin, CreatedMixin
from models.book import Book
from models.reader import Reader

class Prebook(EntityBase, ModifiedMixin, CreatedMixin):
    """
    Element represents a user.
    """
    __tablename__ = 'prebook'
    __table_args__ = {'schema': 'application'}

    # Scalar properties
    book_id = Column(Integer, ForeignKey(Book.id, onupdate="cascade", ondelete="cascade"))
    reader_id = Column(Integer, ForeignKey(Reader.id, onupdate="cascade", ondelete="cascade"))
    booked_date = Column(Date)
    priority = Column(Integer)

    #relational properties
    reader_info = relationship('Reader', backref='prebook')
    book_info = relationship('Book', backref='prebook')
