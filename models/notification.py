"""
This module is notification
"""

from sqlalchemy import Column, ForeignKey
from sqlalchemy import Integer, Date
from sqlalchemy.orm import relationship
from models import EntityBase, ModifiedMixin, CreatedMixin
from models.book import Book
from models.reader import Reader

class Notification(EntityBase, ModifiedMixin, CreatedMixin):
    """
    notification
    """
    __tablename__ = 'notification'
    __table_args__ = {'schema': 'application'}

    # Scalar properties
    reader_id = Column(Integer, ForeignKey(Reader.id, onupdate="cascade", ondelete="cascade"))
    book_id = Column(Integer, ForeignKey(Book.id, onupdate="cascade", ondelete="cascade"))
    due_date = Column(Date)

    #relational properties
    reader_info = relationship('Reader', backref='notification')
    book_info = relationship('Book', backref='notification')
