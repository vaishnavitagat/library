"""
Element represents the Reader type
"""
from library.models.db_enums import MembershipTypeEnum
from sqlalchemy import Column
from sqlalchemy import Integer, Float, Enum
from models import EntityBase, ModifiedMixin, CreatedMixin


class ReaderType(EntityBase, ModifiedMixin, CreatedMixin):
    """
    Element represents a user.
    """
    __tablename__ = 'reader_type'
    __table_args__ = {'schema': 'application'}

    # Scalar properties
    id = Column(Integer, primary_key=True)
    reader_type = Column(Enum(MembershipTypeEnum))
    max_books_quota = Column(Integer)
    period_of_book_allotment = Column(Integer)
    no_times_of_book_renewal = Column(Integer)
    fine_cost_per_day = Column(Float)
    all_readertype_attributes = ['id', 'reader_type', 'max_books_quota', 'period_of_book_allotment', 'no_times_of_book_renewal', 'fine_cost_per_day']

    #relational properties
    #reader = relationship('reader', backref='reader_type')
