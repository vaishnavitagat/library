"""
This module contains account details
"""
#todo all docstrings and comments should be proper - office
#todo correct all the lintys - office
from sqlalchemy import Column
from sqlalchemy import Integer
from models import EntityBase, ModifiedMixin, CreatedMixin


class Account(EntityBase, ModifiedMixin, CreatedMixin):
    """
    Element represents a user.
    """
    __tablename__ = 'account'
    __table_args__ = {'schema': 'application'}

    # Scalar properties
    balance = Column(Integer, nullable=False)
    account_no = Column(Integer, nullable=False, unique=True)
