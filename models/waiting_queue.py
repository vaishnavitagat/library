"""
Element represents the waiting queue for a book and reader's priority
"""
from sqlalchemy import Column, ForeignKey
from sqlalchemy import Integer
from sqlalchemy.orm import relationship
from models import EntityBase, ModifiedMixin, CreatedMixin
from models.book import Book
from models.reader import Reader

class WaitingQueue(EntityBase, ModifiedMixin, CreatedMixin):
    """
    Element represents the waiting queue object
    """
    __tablename__ = 'waiting_queue'
    __table_args__ = {'schema': 'application'}

    # Scalar properties
    reader_id = Column(Integer, ForeignKey(Reader.id, onupdate="cascade", ondelete="cascade"))
    book_id = Column(Integer, ForeignKey(Book.id, onupdate="cascade", ondelete="cascade"))
    queue_no = Column(Integer)

    #relational properties
    reader = relationship('Reader', backref='waiting_queue')
    book = relationship('Book', backref='waiting_queue')
