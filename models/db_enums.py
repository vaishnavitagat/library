import enum


class MembershipTypeEnum(enum.Enum):
    """
    contains three type of memberships as enums
    """
    #  based on payments
    platinum = 'Platinum'
    gold = 'Gold'
    silver = 'Silver'

    @staticmethod
    def get_membership_enum(membership):
        return MembershipTypeEnum.__members__.get(membership)


class IssuedBookStatusEnum(enum.Enum):
    """
    this is an enum for status of book
    """
    issued = 'Issued'
    delayed = 'Delayed'
    renewed = 'Renewed'


class BookCategoryEnum(enum.Enum):
    """
    This module contains enum for category of books
    """
    science = 'Science'
    fiction = 'Fiction'
    spiritual = 'Spiritual'
    self_help = 'Self Help'
    educational = 'Educational'
    biography = 'Biography'
    autobiography = 'Autobiography'
    religious = 'Religious'
    mystery = 'Mystery'
    magazines = 'Magazines'


class PeriodOfMembership(enum.Enum):
    """
    this is enum for membership period
    """
    # in terms of months
    annual = 'Annual'
    half_year = 'Half year'
    quarter = 'Quarter'
    month = 'Month'

    @staticmethod
    def get_membership_period_enum(period):
        period = period.replace(' ', '_')
        return PeriodOfMembership.__members__.get(period)


class TransactionComment(enum.Enum):
    """
    this is transaction type enum
    """
    #  in terms of months
    reader_registration = 'reader_registration'
    membership_renewal = 'membership_renewal'
    fine = 'fine'
    new_book_arrival = 'new_book_arrival'


class TransactionType(enum.Enum):
    """
    This is a Transaction Enum to show credit or debit
    """
    credit = 'Credit'
    debit = 'Debit'
