"""
Element represents rating details
"""
from sqlalchemy import Column, ForeignKey
from sqlalchemy import Integer, String
from sqlalchemy.orm import relationship
from models import EntityBase, ModifiedMixin, CreatedMixin
from models.reader import Reader
from models.book import Book

class Rating(EntityBase, ModifiedMixin, CreatedMixin):
    """
    Element represents a rating module
    """
    __tablename__ = 'rating'
    __table_args__ = {'schema': 'application'}
    __table_args__ = {'schema': 'application'}

    # Scalar properties
    stars = Column(Integer, nullable=False)
    book_id = Column(Integer, ForeignKey(Book.id, onupdate="cascade", ondelete="cascade"))
    reader_id = Column(Integer, ForeignKey(Reader.id, onupdate="cascade", ondelete="cascade"))
    review = Column(String(50))

    # relational properties
    book_rating = relationship('Book', backref='rating')
    reader_rating = relationship('Reader', backref='rating')
