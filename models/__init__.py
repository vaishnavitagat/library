"""
This module contains the base class for db models and various base mixins classes.
"""
from datetime import datetime
from enum import Enum
from sqlalchemy import Column
from sqlalchemy import Integer, DateTime
from sqlalchemy.ext.declarative import declarative_base

Base = declarative_base()


class EntityBase(Base):
    """
    Element represents the key column for all tables.
    """
    __abstract__ = True

    # Scalar properties
    id = Column(Integer, primary_key=True, autoincrement=True)

    @staticmethod
    def convert_obj_to_dict(obj, attri_list):
        obj_dict = {}
        for attribute in attri_list:
            obj_attri = getattr(obj, attribute)
            if isinstance(obj_attri, Enum):
                obj_attri = obj_attri.value
            obj_dict[attribute] = obj_attri
        return obj_dict


class ModifiedMixin():
    """
    Element represents the modification related set of columns
    """
    # Scalar properties
    modified = Column(DateTime,
                      default=datetime.now(),
                      onupdate=datetime.now())


class CreatedMixin():
    """
    Element represents the creation related set of columns
    """
    # Scalar properties
    created = Column(DateTime,
                     default=datetime.now(),
                     onupdate=datetime.now())




