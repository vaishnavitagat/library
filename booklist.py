"""
    contains all books list and methods to choose a random book
"""
import random
class BookList():
    """
    contains all books list
    """
    list_of_books = {'Science': ['The Universe in a Nutshell',
                                 'Relativity: The Special and General Theory',
                                 'The Selfish Gene',
                                 'One Two Three . . . Infinity',
                                 'What Is Life?',
                                 'The Cosmic Connection',
                                 'The Insect Societies',
                                 'The First Three Minutes',
                                 'Silent Spring',
                                 'Under a Lucky Star'],

                     'Fiction': ['The Kite Runner',
                                 'Invisible Man',
                                 'Romeo and Juliet',
                                 'A Thousand Splendid Suns',
                                 'Life of Pi',
                                 'The Da Vinci Code',
                                 'The Lovely Bones',
                                 'The Help',
                                 'Water for Elephants',
                                 'Hamlet'],

                     'Self Help': ['Think and Grow Rich',
                                   'The Alchemist',
                                   'Flow: The Psychology of Optimal Experience',
                                   'Thinking Fast and Slow',
                                   'The Power of Habit: Why We Do What We Do In Life and Business',
                                   'Awaken the Giant Within',
                                   'Outliers: The Story of Success',
                                   'Design Your Life: Build A Life That Works For You',
                                   'Big Magic',
                                   'The Wisdom of Sundays'],

                     'Educational': ['Machine Learning using Python',
                                     'Python Crash Course',
                                     'Head-First Python',
                                     'Learn Python the Hard Way',
                                     'Python Programming: An Introduction to Computer Science',
                                     'Learning with Python: How to Think Like a Computer Scientist',
                                     'A Byte of Python',
                                     'Fluent Python: Clear, Concise, and Effective Programming',
                                     'Python Cookbook: Recipes for Mastering Python 3',
                                     'Programming Python: Powerful Object-Oriented Programming'],

                     'Biography': ['Steve Jobs',
                                   'Long Walk to Freedom',
                                   'Becoming',
                                   'The men who knew infinity',
                                   'Benjamin Franklin: An American Life',
                                   'Einstein: His Life and Universe',
                                   'The Snowball: Warren Buffett and the Business of Life',
                                   'Life of Swami Vivekananda',
                                   'Elon Musk: How the Billionaire CEO of SpaceX and Tesla is Shaping our Future',
                                   'The Everything Store: Jeff Bezos and the Age of Amazon',
                                   'How to Fail at Almost Everything and Still Win Big: Kind of the Story of My Life'],

                     'Religious': ['Bhagavad Gita',
                                   'Mahabharatha',
                                   'Ramayana',
                                   'Sunderkand',
                                   'Shiv Puran',
                                   'Why I Am a Hindu',
                                   'The Hidden Power of Gayatri Mantra'],

                     'Spiritual': ['Patanjali yoga sutras',
                                   'BKS Iyengar Yoga The Path to Holistic Health: The Definitive Step-by-Step Guide',
                                   'The Secret',
                                   'Everyday Ayurveda : Daily Habits That Can Change Your Life',
                                   'Chakra Balancing Made Simple and Easy',
                                   'How to Eliminate Negative Thoughts and Emotions with One Simple but Powerful Technique'],

                     'Autobiography': ['The Autobiography of Benjamin Franklin',
                                       'Autobiography of a Yogi',
                                       'Mein Kampf',
                                       'Wings of Fire',
                                       'Mahatma Gandhi Autobiography: The Story Of My Experiments With Truth',
                                       'Playing It My Way by Sachin Tendulkar',
                                       'The Story of My Life by Helen Keller'],

                     'Mystery': ['11 Hours',
                                 'Murder on the Orient Express',
                                 'The Woman in the Window',
                                 'The Girl in Room 105',
                                 'Case No. 56',
                                 'And Then There Were None',
                                 'The Best of Sherlock Holmes',
                                 'Murder House',
                                 'Gone Girl',
                                 '1984'],

                     'Magazines': ['Femina',
                                   'Cricket Today',
                                   'Filmfare',
                                   "Reader's Digest",
                                   'Outlook',
                                   'Competition Success Review',
                                   'Sportstar',
                                   'General Knowledge Today']}


    #todo remove this useless function
    @staticmethod
    def get_random_book_by_category(bookcategory):
        """
        method to choose a random book of a particular category which is received as an argument
        """
        book_list_by_category = bookcategory.value
        book_name = random.choice(BookList.list_of_books[book_list_by_category])
        return book_name
