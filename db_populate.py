"""
This is an independent script which populates reader,book, readertype
librarian and account tables in the database.
"""

import random
import string

from libs.utils import get_fake_expiry_date
from faker import Faker
from passlib.hash import sha256_crypt
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker

from booklist import BookList
from models.account import Account
from models.db_enums import BookCategoryEnum
from models.book import Book
from models.librarian import Librarian
from models.reader import Reader, PeriodOfMembership
from models.readertype import ReaderType
from library.models.db_enums import MembershipTypeEnum
from models.book import BookStock

NO_OF_READERS = 50
ALPHABET = string.ascii_letters + string.digits
ENDPOINT = 'postgresql://postgres:hp12@localhost:5432/library_db'
ENGINE = create_engine(ENDPOINT, echo=True)

Session = sessionmaker(bind=ENGINE)
session = Session()

publisher_suffix_list = ['Publishers', 'Press', 'Publishing House', 'Publications', 'Books']

fake = Faker()
book_categories = {category.value: category for __, category in BookCategoryEnum.__members__.items()}

books = []
list_of_books = BookList().list_of_books

for category, category_books in list_of_books.items():
    for category_book in category_books:
        book = Book()
        book.isbn = random.randint(900000000, 999999999)
        book.category = book_categories[category]
        book.book_name = category_book
        book.price = random.randint(100, 1000)
        book.publisher = '{} {}'.format(fake.last_name(), random.choice(publisher_suffix_list))
        book.author = fake.name()
        session.add(book)
        session.commit()
        books.append(book)

for book in books:
    stock = BookStock()
    stock.book_id = book.id
    stock.total_copies = random.randint(5, 50)
    stock.available_copies = stock.total_copies
    session.add(stock)


# Inserting data in librarian table
librarian1 = Librarian()
librarian2 = Librarian()
librarian1.librarian_name = fake.name()
librarian2.librarian_name = fake.name()
session.add(librarian1)
session.add(librarian2)
session.commit()

# Inserting data in account table
account = Account()
account.balance = 5000
account.account_no = 1625344352
session.add(account)
session.commit()

# Inserting data in reader_type table
all_reader_type = []

reader_type_list = [member_type for __, member_type in MembershipTypeEnum.__members__.items()]
for member_type in reader_type_list:
    readerType = ReaderType()
    if member_type.value == MembershipTypeEnum.platinum.value:
        readerType.max_books_quota = 10
        readerType.fine_cost_per_day = 5
        readerType.period_of_book_allotment = 30 #in days
        readerType.no_times_of_book_renewal = 3
        readerType.reader_type = member_type
        session.add(readerType)
        session.commit()
        all_reader_type.append(readerType)

    if member_type.value == MembershipTypeEnum.gold.value:
        readerType.max_books_quota = 5
        readerType.fine_cost_per_day = 10
        readerType.period_of_book_allotment = 15 #in days
        readerType.no_times_of_book_renewal = 2
        readerType.reader_type = member_type
        session.add(readerType)
        session.commit()
        all_reader_type.append(readerType)

    if member_type.value == MembershipTypeEnum.silver.value:
        readerType.max_books_quota = 2
        readerType.fine_cost_per_day = 15
        readerType.period_of_book_allotment = 7 #in days
        readerType.no_times_of_book_renewal = 1
        readerType.reader_type = member_type
        session.add(readerType)
        session.commit()
        all_reader_type.append(readerType)
session.commit()

all_readers = []
# Inserting data reader table
for i in range(NO_OF_READERS):
    reader = Reader()
    reader.name = fake.name()
    reader.reader_type_id = (random.choice(all_reader_type)).id
    reader.phone_no = ''.join([str(random.randint(0, 9)) for __ in range(10)])
    reader.email_id = '{}@dummymail.com'.format(reader.name.replace(' ', '.'))
    reader.period_of_membership = random.choice(list(PeriodOfMembership))  # my easy way of doing it
    if reader.period_of_membership.value == PeriodOfMembership.annual.value:
        reader.expiry_date = get_fake_expiry_date(PeriodOfMembership.annual)
    elif reader.period_of_membership.value == PeriodOfMembership.half_year.value:
        reader.expiry_date = get_fake_expiry_date(PeriodOfMembership.half_year)
    elif reader.period_of_membership.value == PeriodOfMembership.quarter.value:
        reader.expiry_date = get_fake_expiry_date(PeriodOfMembership.quarter)
    else:
        reader.expiry_date = get_fake_expiry_date(PeriodOfMembership.month)
    reader.password_hash = sha256_crypt.hash('Lib@12345') #encrypted version of default password
    reader.last_login = fake.date_between(start_date=1-1-2000, end_date='today')
    reader.hint = ''  # since none is not a string and hint accepts only string changed it to empty string
    session.add(reader)
    session.commit()
    all_readers.append(reader)
session.commit()
