class DbFetchFailureException(Exception):
    """
    Raise this exception when there is exception in db operations
    """
    pass


class InvalidInputException(Exception):
    """
    Raise this exception when there is invalid input given by the user
    """
    pass