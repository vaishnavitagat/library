"""
all utilities
"""
import json, re
from datetime import datetime, date
from faker import Faker
from http import HTTPStatus

import traceback
import functools
import logging
from models.db_enums import PeriodOfMembership
from settings import fromemail, password, mail
from libs.custom_exceptions import InvalidInputException

fake = Faker()


class DateTimeEncoder(json.JSONEncoder):
    """
    overloading Datetime class to convert date time to json
    """

    def default(self, o):
        """
        overloading Datetime class to convert date time to json
        """
        if isinstance(o, datetime) or isinstance(o, date):
            return o.isoformat()
        return json.JSONEncoder.default(self, o)


def get_membership_expiry(membership_period):
    if membership_period == PeriodOfMembership.annual:
        return fake.date_between(start_date='today', end_date='+12M')
    if membership_period == PeriodOfMembership.half_year:
        return fake.date_between(start_date='today', end_date='+6M')
    if membership_period == PeriodOfMembership.quarter:
        return fake.date_between(start_date='today', end_date='+3M')


def is_bad_password(password):
    """
    password should contain
    1. atleast one Capital letter
    2. atleast one small letter
    3. atleast one numerical
    4. min length 8 chars
    5. max length 60 chars
    6. No special except - @,#,$,%,&,!,*
    """
    special_char = ['@', '#', '$', '%', '&', '!', '*']
    msg = 'Bad password: Please make sure you have 8-20characters,' \
          ' at least 1 number, at least 1 Capital letter, ' \
          'at least 1 of these special characters-@ # $ % & ! * '
    if len(password) < 8:
        raise InvalidInputException(msg)
    if len(password) > 20:
        raise InvalidInputException(msg)
    if not any(char.isdigit() for char in password):
        raise InvalidInputException(msg)
    if not any(char.isupper() for char in password):
        raise InvalidInputException(msg)
    if not any(char in special_char for char in password):
        raise InvalidInputException(msg)
    return ''


def email_validation(email):
    # shd contain one of these domains [.com, .org, .in, .edu, .net, .int]
    # better use a regex
    # shd contain @
    # shd have username before @
    # after @ there should be domain
    pattern = '^[a-z,0-9]+@[a-z]+.com$'
    valid_email = re.match(pattern, email)
    if not valid_email:
        return False
    return True


def send_email(subject, to, msg):
    # SUBJECT = "Hello!"
    # msg = "This message was sent with Python's smtplib."
    message = 'Subject: {}\n\n{}'.format(subject, msg)
    mail.ehlo()
    mail.starttls()
    mail.login(fromemail, password)
    try:
        mail.sendmail(fromemail, to, message)
        mail.close()
        print('log mail sent to', to, 'for', subject)
    except:
        print('log mail not sent')


def exception_catcher(func):
    @functools.wraps(func)
    def wrapper_exception_catcher(*args, **kwargs):
        try:
            result = func(*args, **kwargs)
        except Exception as e:
            logging.exception("Exception occurred", exc_info=True)
            print("Unexpected exception occurred while executing {}. Error - {}".format(func.__name__, e))
            traceback.print_exc()
            return "Unexpected exception occurred while executing {}".format(func.__name__), HTTPStatus.INTERNAL_SERVER_ERROR
        return result

    return wrapper_exception_catcher


def get_fake_expiry_date(membership):
    if membership == PeriodOfMembership.annual:
        return fake.date_between(start_date='today', end_date='+12M')
    if membership == PeriodOfMembership.half_year:
        return fake.date_between(start_date='today', end_date='+6M')
    if membership == PeriodOfMembership.quarter:
        return fake.date_between(start_date='today', end_date='+3M')
    if membership == PeriodOfMembership.month:
        return fake.date_between(start_date='today', end_date='+1M')