from models.book import Book
from settings import engine, session


class BookRepo:
    @staticmethod
    def get_book_query():
        query = session.query(Book)
        return query

    @staticmethod
    def get_all_books():
        query = BookRepo.get_book_query()
        return query.all()

    @staticmethod
    def get_book_by_id(book_id):
        book = session.query(Book).get(book_id)
        return book

    @staticmethod
    def get_book_by_category(category):
        try:
            books = BookRepo.get_book_query().filter(Book.category == category).all()
        except Exception as ex:
            session.close()
            return 'Please enter a valid book category!'
        return books

    @staticmethod
    def get_book_by_author(author):
        books = BookRepo.get_book_query().filter(Book.author == author).all()
        return books

