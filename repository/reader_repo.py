from settings import session
from models.reader import Reader, ReaderType
from library.libs.utils import get_fake_expiry_date
from passlib.hash import sha256_crypt
from library.models.db_enums import MembershipTypeEnum


class ReaderRepo:
    @staticmethod
    def get_reader_query():
        query = session.query(Reader)
        return query

    @staticmethod
    def get_all_readers():
        query = ReaderRepo.get_reader_query()
        return query.all()

    @staticmethod
    def get_reader_by_username(username):
        reader = ReaderRepo.get_reader_query().filter(Reader.name == username).scalar()
        return reader

    @staticmethod
    def get_readertypeid(reader_type):
        reader_type = MembershipTypeEnum.get_membership_enum(reader_type)
        reader_type_id = session.query(ReaderType).filter(ReaderType.reader_type == reader_type).one().id
        return reader_type_id

    @staticmethod
    def add_reader(content,membership_period_enum, reader_type_id):
        new_reader = Reader()
        new_reader.name = content['username']
        new_reader.reader_type_id = reader_type_id
        new_reader.phone_no = content['phone_no']
        new_reader.email_id = content['email_id']
        new_reader.expiry_date = get_fake_expiry_date(
            membership_period_enum)
        new_reader.password_hash = sha256_crypt.hash(content['password'])
        new_reader.hint = content['hint']
        new_reader.period_of_membership = content['membership_period'].replace(' ', '_')

        session.add(new_reader)
        session.commit()

        return new_reader

