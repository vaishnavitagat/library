"""
This is the main app module which contains the routes and APIs.
"""
import json
import datetime
import random
from flask import Flask, render_template, redirect, url_for, request
from passlib.hash import sha256_crypt
from sqlalchemy.orm import sessionmaker
from sqlalchemy import create_engine, or_
from sqlalchemy.orm.exc import NoResultFound, MultipleResultsFound
from libs.utils import DateTimeEncoder,send_email, exception_catcher
from models.book import Book
from models.reader import Reader
from models.issued_books import IssuedBooks
from models.book import BookStock
from models.readertype import ReaderType
from models.waiting_queue import WaitingQueue
from models.rating import Rating
from models.db_enums import IssuedBookStatusEnum
from models.account import Account
from models.transaction import Transaction
from models.db_enums import  TransactionType, TransactionComment
from models.reserved_book import ReservedBook
from library.service.book_service import BookService
from library.service.reader_service import ReaderService
from library.libs.custom_exceptions import DbFetchFailureException
from http import HTTPStatus

endpoint = 'postgresql://postgres:hp12@localhost:5432/library_db'
engine = create_engine(endpoint, echo=True)
Session = sessionmaker(bind=engine)
session = Session()

app = Flask(__name__)  # creating the Flask class object
app.config['SQLALCHEMY_DATABASE_URI'] = 'postgresql://postgres:hp12@localhost:5432/library_db'


# working fine
@app.route('/welcome/')
@exception_catcher
def welcome():
    """
    this is welcome route
    """
    redirect(url_for('login'))
    return render_template('welcome.html')


# working fine
@app.route('/book/list/')
@exception_catcher
def get_book_list():
    """
    api to list all books
    """
    try:
        book_list = BookService.get_book_list()
    except DbFetchFailureException as ex:
        return 'DbFetchFailureException occurred while getting book list. Error - {}'.format(ex), HTTPStatus.INTERNAL_SERVER_ERROR
    except Exception as ex:
        return 'Exception occurred while getting book list. Error - {}'.format(ex), HTTPStatus.INTERNAL_SERVER_ERROR

    return json.dumps(book_list, cls=DateTimeEncoder, indent=4), HTTPStatus.OK


@app.route('/reader/list/')
@exception_catcher
def get_reader_list():
    """
    api to return reader list
    """
    try:
        reader_list = ReaderService.get_reader_list()
    except DbFetchFailureException as ex:
        return 'DbFetchFailureException occurred while getting reader list. Error - {}'.format(ex), HTTPStatus.INTERNAL_SERVER_ERROR
    except Exception as ex:
        return 'Exception occurred while getting reader list. Error - {}'.format(ex), HTTPStatus.INTERNAL_SERVER_ERROR
    return json.dumps(reader_list, cls=DateTimeEncoder, indent=4), HTTPStatus.OK


# working fine
@app.route('/book/<book_id>/')
@exception_catcher
def book_by_id(book_id):
    """
    api to fetch book by ID
    """
    try:
        book_dict = BookService.get_book_by_id(book_id)
    except DbFetchFailureException as ex:
        return 'DbFetchFailureException occurred while getting book list. Error - {}'.format(ex), HTTPStatus.INTERNAL_SERVER_ERROR
    except Exception as ex:
        return 'Exception occurred while getting book by id. Error - {}'.format(ex), HTTPStatus.INTERNAL_SERVER_ERROR

    return json.dumps(book_dict, cls=DateTimeEncoder, indent=4)


@app.route('/book_by_category/<category>')
@exception_catcher
def book_by_category(category):
    """
    api to fetch all books in a category
    """
    try:
        book_list = BookService.get_book_by_category(category)
    except DbFetchFailureException as ex:
        return 'DbFetchFailureException occurred while getting book list. Error - {}'.format(ex), HTTPStatus.INTERNAL_SERVER_ERROR
    except Exception as ex:
        return 'Exception occurred while getting book by category. Error - {}'.format(ex), HTTPStatus.INTERNAL_SERVER_ERROR
    return json.dumps(book_list, cls=DateTimeEncoder, indent=4)


# working fine
@app.route('/book_by_author/<author>')
@exception_catcher
def book_by_author(author):
    """
    api to fetch all books by an author
    """
    try:
        book_list = BookService.get_book_by_author(author)
    except DbFetchFailureException as ex:
        return 'DbFetchFailureException occurred while getting books by author. Error - {}'.format(ex), HTTPStatus.INTERNAL_SERVER_ERROR
    except Exception as ex:
        return 'Exception occurred while getting book by author. Error - {}'.format(ex), HTTPStatus.INTERNAL_SERVER_ERRO

    return json.dumps(book_list, cls=DateTimeEncoder, indent=4)


# working fine
@app.route('/reader/login/', methods=['POST'])
@exception_catcher
def login():
    """
    User login api
    #todo after login what- return auth token(this token shd be included in every request)
    """
    content = request.get_json()
    username = content['username']
    password = content['password']
    # raise Exception("My dummy exception")
    try:
        reader = session.query(Reader).filter(Reader.name == username).one()
    except MultipleResultsFound as ex:
        return 'Multiple reader exist with same name {}. Please contact admin.'.format(username)
    except NoResultFound:
        return 'You are not a registered user. Please register yourself'

    if reader:
        reader_password = reader.password_hash
        if not sha256_crypt.verify(password, reader_password):
            return 'please enter a valid password'
        if reader.expiry_date < datetime.date.today():
            return 'Sorry your membership has been expired. Please renew it'

        reader.last_login = datetime.date.today()

        session.commit()
        return 'Welcome {}! You are now logged in.'.format(username)


# working fine
@app.route('/reader/register', methods=['POST'])
@exception_catcher
def register_new_reader():
    """
    API to register new user
    validate password
    check if there  is already an existing user with same username , then prompt to choose new username
    else add this new reader to reader table
    """
    content = request.get_json()

    try:
        new_reader = ReaderService.register_new_reader(content)
    except DbFetchFailureException as ex:
        return 'DbFetchFailureException occurred while getting book list. Error - {}\n {}'.format(ex), HTTPStatus.INTERNAL_SERVER_ERROR
    except Exception as ex:
        return 'Exception occurred while registering new reader. Error - {}'.format(ex), HTTPStatus.INTERNAL_SERVER_ERROR

    subject = 'Successfully registration'
    msg = 'You got successfully registered to our library! You can start reading books of your choice'
    try:
        send_email(subject, new_reader.email_id, msg)
    except Exception as ex:
        print('log msg - failed to send mail. Error - {}', ex)

    return 'Congrats {}! You are a member now. You can start reading books of your choice.'.format(content['username']), HTTPStatus.CREATED


# working fine
@app.route('/reader/borrow_book/', methods=['POST'])
@exception_catcher
def borrow_book():
    """
    API to issue the requested book by a reader based on meeting the below conditions
    1. if reader is registered
    2. if book exists in library
    3. should not issue same bookid to same reader
    4. check the no of books which can be issued to this reader
    5. if the book is available issue, else add the reader for waiting queue
    6. check reader membership validity
    """
    content = request.get_json()
    book_id = content['book_id']
    reader_id = content['reader_id']

    # check if the book exists in the library
    try:
        bookstock = session.query(BookStock).filter(BookStock.book_id == book_id).one()
    except NoResultFound:
        return 'No book with id {} found. Please contact admin'.format(book_id)
    except MultipleResultsFound:
        return 'Multiple books found with id {}. Please contact admin'.format(book_id)

    book_name = bookstock.book_details.book_name

    # checking if reader is a member of library
    reader = session.query(Reader).get(reader_id)
    if not reader:
        return 'You are not a registered user of this library. Please register'

    if reader.expiry_date < datetime.date.today():
        return 'Your membership has been expired. Please renew it.'

    reader_reading_same_book = session.query(IssuedBooks).filter(IssuedBooks.reader_id == reader_id). \
        filter(IssuedBooks.book_id == book_id).filter(or_(IssuedBooks.status == IssuedBookStatusEnum.issued,
                                                          IssuedBooks.status == IssuedBookStatusEnum.renewed)).scalar()
    if reader_reading_same_book:
        return 'You cant take the same book again'

    # check the no of books which can be issued to this reader
    reader_type = session.query(ReaderType).get(reader.reader_type_id)
    if not reader_type:
        return 'Reader type with id {} not found. Please contact admin'.format(reader_id)

    issued_no_of_books_to_this_reader = session.query(IssuedBooks).filter(IssuedBooks.reader_id == reader_id). \
        filter(or_(IssuedBooks.status == IssuedBookStatusEnum.issued,
                   IssuedBooks.status == IssuedBookStatusEnum.renewed)).count()
    if issued_no_of_books_to_this_reader >= reader_type.max_books_quota:
        return 'Sorry you have already taken maximum no of books({}). \n' \
               'If you are not a platinum user please upgrade your membership to borrow more books'.format(
            issued_no_of_books_to_this_reader)

    if bookstock.available_copies == 0:
        reader_in_wq = session.query(WaitingQueue).filter(WaitingQueue.reader_id == reader.id).filter(
            WaitingQueue.book_id == book_id).scalar()
        if reader_in_wq:
            return 'You are already in waiting queue for this book!'
        waiting_reader = WaitingQueue()
        waiting_reader.reader_id = reader_id
        waiting_reader.book_id = book_id
        session.add(waiting_reader)
        session.commit()
        # todo to ask user if he wants to get added to waiting queue - Send a response with a link. Link can contain user id.
        # When clicked on link, user can be added
        return 'added you to waiting queue'

    book_issued = issue_book(book_id, reader_id, reader_type)
    bookstock.available_copies -= 1
    session.merge(bookstock)
    session.add(book_issued)
    session.commit()
    due_date = book_issued.due_date
    subject = '"{}" book issued'.format(book_name)
    to = reader.email_id
    msg = 'Issued "{} " book to you. Due date to return this book is '.format(book_name, due_date)
    send_email(subject, to, msg)
    return 'Issued book "{}" to reader {}'.format(book_name, reader.name)


# working fine
@exception_catcher
def issue_book(book_id, reader_id, reader_type, status=IssuedBookStatusEnum.issued, is_returned=False):
    """
    API to issue book and update the stock availability of the book
    """
    # issuing book
    book = IssuedBooks()
    book.reader_id = reader_id
    book.book_id = book_id
    book.issued_date = datetime.date.today()
    book.due_date = datetime.date.today() + datetime.timedelta(days=reader_type.period_of_book_allotment)
    book.status = status
    book.is_returned = is_returned
    return book


# working fine
@app.route('/reader/renew_book/', methods=['POST'])
@exception_catcher
def renew_book():
    # get the book to renew
    # check how many times the book has been renewed for same book for same reader
    # (what if the book is renewed twice and returned then again borowwed and renewed again, count of the renewal wil increase
    content = request.get_json()
    reader_id = content['reader_id']
    book_id = content['book_id']
    try:
        issued_book_rec = session.query(IssuedBooks).filter(IssuedBooks.reader_id == reader_id).filter(
            IssuedBooks.book_id == book_id).filter(
            or_(IssuedBooks.status == IssuedBookStatusEnum.issued, IssuedBooks.status == IssuedBookStatusEnum.renewed)). \
            filter(IssuedBooks.is_returned == False).one()
    except NoResultFound:
        return 'You haven\'t borrowed this book. Please renew the book which you borrowed'
    except MultipleResultsFound:
        return 'Multiple books were found to renew. Please contact admin'
    else:
        # checking the no of times reader has renewed
        reader = session.query(Reader).get(reader_id)
        reader_renewal_qouta = reader.reader_type_details.no_times_of_book_renewal
        reader_book_allowment_period = reader.reader_type_details.period_of_book_allotment

        reader_renewal_count = session.query(IssuedBooks).filter(IssuedBooks.reader_id == reader_id).filter(
            IssuedBooks.book_id == book_id).filter(IssuedBooks.modified >= (
                datetime.datetime.today() - datetime.timedelta(days=reader_book_allowment_period))). \
            filter(IssuedBooks.status == IssuedBookStatusEnum.renewed).count()

        if reader_renewal_count >= reader_renewal_qouta:  # minus 1 because there will be one is_returned=false which should be counted
            return 'Sorry you have exceeded you renewal quota({}). ' \
                   'Please return the book and get it reissued based on the availability'.format(reader_renewal_qouta)
        else:
            issued_book_rec.is_returned = True
            new_issued_rec = issue_book(book_id, reader_id, reader.reader_type_id, status=IssuedBookStatusEnum.renewed)
            session.merge(new_issued_rec)
            session.merge(issued_book_rec)
            session.commit()
            return 'Renewed your book, you can continue reading it.'


# working fine
@app.route('/reader/return_book', methods=['POST'])
@exception_catcher
def return_book():
    """
    API to return the book which internally calls accept book function
     # shd change renewal to return before renewal and add new column
    """
    content = request.get_json()
    book_to_return = content['book_id']
    reader_id = content['reader_id']

    try:
        book = session.query(IssuedBooks).filter(IssuedBooks.book_id == book_to_return).filter(
            IssuedBooks.reader_id == reader_id).filter(or_(IssuedBooks.status == IssuedBookStatusEnum.issued,
                                                           IssuedBooks.status == IssuedBookStatusEnum.renewed)).filter(
            IssuedBooks.is_returned.is_(False)).one()

    except NoResultFound:
        return 'Invalid book return. Please return the book you borrowed'
    except MultipleResultsFound:
        return 'Found multiple books to return with id {}. Please contact admin'.format(book_to_return)
    msg = accept_return_of_book(book_to_return, reader_id, book)
    if msg is not None:
        return msg
    return 'Book {} returned'.format(book_to_return)


# working fine
@exception_catcher
def accept_return_of_book(book_id, reader_id, accept_book):
    """
    function which accept the book returned by the reader
    """

    if accept_book.due_date < datetime.date.today():
        pay_fine(accept_book.due_date, reader_id, accept_book)
    accept_book.is_returned = True
    try:
        available_copies = session.query(BookStock).filter(BookStock.book_id == book_id).one()
        available_copies.available_copies += 1
        session.merge(available_copies)
        session.merge(accept_book)
        session.commit()
    except NoResultFound:
        return 'No book with id {} found.'.format(book_id)
    except MultipleResultsFound:
        return 'Multiple books found with id {}'.format(book_id)
    except TypeError:
        return 'Available copies is incorrect for book id {} is  {}'.format(book_id, available_copies)
    else:
        return None


# working fine
@exception_catcher
def pay_fine(due_date, reader_id, issued_book_obj):
    # while returning the book collect fine
    # collect fine and add to transcation and acount bank on per day basis
    reader = session.query(Reader).get(reader_id)
    fine = reader.reader_type_details.fine_cost_per_day
    delta = str(datetime.date.today() - due_date).replace(' days, 0:00:00', '')
    issued_book_obj.delayed_days = delta
    total_fine = fine * int(delta)
    issued_book_obj.fine_amt = total_fine
    session.merge(issued_book_obj)
    print('log msg - you have to pay ₹', total_fine)

    trans = Transaction()
    account = session.query(Account).first()
    trans.account_no = account.account_no
    trans.reader_id = reader_id
    trans.amount = total_fine
    trans.comment = TransactionComment.fine
    trans.transaction_type = TransactionType.credit
    trans.opening_balance = account.balance
    account.balance += total_fine
    trans.closing_balance = account.balance
    session.add(trans)

    # add account balance
    session.merge(account)
    session.commit()
    subject = 'Fine paid'
    to = reader.email_id
    msg = 'You paid a fine of {}₹ for delay in returning of book {} which had to be returned by {} '.format(total_fine,
                                                                                                            issued_book_obj.book_id,
                                                                                                            due_date)
    send_email(subject, to, msg)
    return


# working fine
@app.route('/reader/review', methods=['POST'])
@exception_catcher
def review_rate_book():
    """
    api by which reader can review and rate a book
    """
    content = request.get_json()
    reader_id = content['reader_id']
    book_to_review = content['book_id']
    stars = content['stars']
    review_comment = content.get('comment')  # non mandatory

    try:
        if stars > 5 or stars < 0:
            return 'Please rate from 0-5 only'
    except TypeError:
        return 'Please enter only in integers 0-5 only'

    issued_book = session.query(IssuedBooks).filter(IssuedBooks.reader_id == reader_id).filter(
        IssuedBooks.book_id == book_to_review).first()
    if issued_book is None:
        return 'Please review the book you have read'

    rate = Rating()
    rate.reader_id = reader_id
    rate.stars = stars
    rate.review = review_comment
    rate.book_id = book_to_review
    session.add(rate)
    session.commit()
    return 'Successfully updated your review. Please read more books'


# working fine
@app.route('/reader/reservebook/', methods=['POST'])
@exception_catcher
def reserve_book():
    # check if book is in available status
    # check waiting queue if there are readers waiting for this book
    # check if user is already in wq and trying to reserve book
    # check if book is already issued to user
    # But u should put some restrictions like once booked u have to issue within 2 days,
    # else book will be available for others to issue.
    # todo remove reserved book record after issuing or if pickup date is expired
    content = request.get_json()
    book_id = content['book_id']
    reader_id = content['reader_id']
    add_to_waiting_queue = content['add_to_waiting_queue']
    try:
        if session.query(WaitingQueue).filter(WaitingQueue.book_id == book_id).filter(
                WaitingQueue.reader_id == reader_id).one():
            return 'You are already in waiting queue for this book {}'.format(book_id)
    except NoResultFound:
        available_copies = session.query(BookStock).filter(BookStock.book_id == book_id).one().available_copies
        if available_copies > 0:
            if session.query(IssuedBooks).filter(IssuedBooks.book_id == book_id).filter(
                    IssuedBooks.reader_id == reader_id).filter(IssuedBooks.is_returned == False).scalar():
                return 'You have already been issued this book'

            reserve = ReservedBook()
            reserve.book_id = book_id
            reserve.reader_id = reader_id
            reserve.reserved_date = datetime.date.today()
            reserve.pickup_date = datetime.date.today() + datetime.timedelta(2)
            session.add(reserve)
            session.commit()
            return 'Book {} is reserved. Please collect it within {}'.format(book_id, reserve.pickup_date)
        else:
            if add_to_waiting_queue:
                adding_to_waiting_queue = WaitingQueue()
                adding_to_waiting_queue.reader_id = reader_id
                adding_to_waiting_queue.book_id = book_id
                session.add(adding_to_waiting_queue)
                session.commit()
                return 'Added you to waiting queue'
            return 'This book is not available. Please try later'


# suggest_books_to_buy()
@app.route('/reader/suggest_a_book', methods=['POST'])
@exception_catcher
def suggest_book():
    """
    api for reader to suggest books to procure
    """
    # get book name and author
    # todo notify librarian
    # todo store all suggested books and notify the librarian
    content = request.get_json()
    suggested_book = content['suggested_book']
    author = content['author']
    print('Thank you! Book {} by author {} suggestion has been received and will be notified to our librarian. Thank you!'.format(suggested_book,author))
    #return suggested_book, author
    return 'Thank you! Book {} by author {} suggestion has been received and will be notified to our librarian. Thank you!'.format(suggested_book,author)


@app.route('/librarian/buy_book', methods=['POST'])
@exception_catcher
def buy_book():
    """
    needs to be done by librarian,
    buy the book and notify the readers - done
    deduct the money from account balance as per the no of copies- done
    add book to stock
    fill in the details in book table
    """
    content = request.get_json()
    book_name = content['book_name']
    category = content['category']
    price = content['price']
    author = content['author']
    publisher = content['publisher']
    copies_ordered = content['copies_ordered']
    account_no = content['account_no']


    try:
        session.query(Book).filter(Book.book_name == book_name).one()
    except NoResultFound:
        # adding a new book to library
        subject = 'Hurray New book "{}" has Arrived!!'.format(book_name)
        msg = '"' + book_name + '"' + ' book has arrived! Hurry to collect your copy'
        isbn = random.randint(900000000, 999999999)
        new_book = Book()
        new_book.book_name = book_name
        new_book.category = category
        new_book.isbn = isbn
        new_book.price = price
        new_book.author = author
        new_book.publisher = publisher
        session.add(new_book)

        # updating stock
        get_book_id = session.query(Book).filter(Book.book_name == book_name).one()
        new_book_stock = BookStock()
        new_book_stock.book_id = get_book_id.id
        new_book_stock.total_copies = copies_ordered
        new_book_stock.available_copies = copies_ordered
        session.add(new_book_stock)

    else:
        # notify all readers that there is an extra copy of the book
        subject = 'Hurray extra copy of "{} " has Arrived!!'.format(book_name)
        msg = 'Have you been waiting for "{}"? Well now we have more copies of it. Hurry to collect your copy'.format(book_name)
        #update stock
        check_book_stock = Book.stock
        check_book_stock.total_copies += copies_ordered
        check_book_stock.available_copies += copies_ordered

    last_acc = session.query(Account).order_by(Account.id.desc()).first()
    previous_balance = last_acc.balance
    last_acc.balance -= price * copies_ordered
    session.merge(last_acc)

    transc = Transaction()
    transc.account_no = account_no
    transc.amount = price * copies_ordered
    transc.transaction_type = TransactionType.debit
    transc.comment = TransactionComment.new_book_arrival
    transc.opening_balance = previous_balance
    transc.closing_balance = previous_balance - (price * copies_ordered)
    session.add(transc)
    session.commit()

    # emailing all readers
    all_readers = session.query(Reader).all()
    for reader in all_readers:
        email_id = reader.email_id
        send_email(subject, email_id, msg)

    return 'Successfully added {} book to library.'.format(book_name)


@exception_catcher
def notifier():
    """
    Notify when the due date is next day
    notify when membership expires
    notify when book is available
    notify all readers when a new book is introduced
    notify when book is issued
    notify when book is returned
    notify when new reader registers
    notify when password is changed
    notify when wrong password is entered thrice
    notify prebooking
    notify when reader is add to waiting queue
    check if reserved book pickup date exceeded and send appropriate mail
    # todo use celery for background tasks like checking due date , expiry date - after 3tier conversion
    """
    i = session.query(IssuedBooks).filter(IssuedBooks.due_date <= datetime.date.today()).all()
    for due in i:
        print('the date is', due.due_date)
        print('the reader id is', due.reader_id)
        # pay_fine()


# notifier()
# Priority order
# todo 3tier conversation
# todo put all enum in enums
# todo httpstatus code should be returned
# todo encrypt credentials file
# todo json validation for all post apis
# todo email validation from vicks
# todo email validation for other domains - check vicks
# todo appropriate filter one/all/one_or_none
# todo where all possible use relationship instead of quering
# todo add logs using logger
# todo use auth token
# todo decorator to check token validity
# todo use celery for background tasks like checking due date , expiry date - after 3tier conversion
# todo waiting queue processing and issuing books to them in wq
# todo Waiting queue is for those books for which there is no available copies.
# todo Unit test

# Below tasks do in any order, once all the above things are done.
# todo reset_passwd()/forgot_passwd() - office
# todo notify librarian to buy suggested_books
# todo write a cleanup for unwanted records
# todo change issuedook status to delayed when due date is over
# todo to ask user if he wants to get added to waiting queue - Send a response with a link. Link can contain user id.
# todo remove reserved book record after issuing or if pickup date is expired
# todo store all suggested books and notify the librarian


if __name__ == '__main__':
    app.run(debug=True)
