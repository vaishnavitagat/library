from library.libs.utils import is_bad_password, email_validation, send_email
from libs.custom_exceptions import DbFetchFailureException, InvalidInputException
from models.db_enums import PeriodOfMembership
from library.models.db_enums import MembershipTypeEnum
from repository.reader_repo import ReaderRepo


class ReaderService:
    @staticmethod
    def get_reader_list():
        try:
            readers = ReaderRepo.get_reader_query()
        except Exception as ex:
            print('Unexpected error occurred while fetching all books. Error - {}'.format(ex))
            raise DbFetchFailureException('Unable to fetch db records.')

        reader_list = []
        for reader in readers:
            reader_dict = reader.convert_obj_to_dict(reader, reader.all_reader_attributes)
            reader_list.append(reader_dict)
        return reader_list

    @staticmethod
    def register_new_reader(content):
        username = content['username']
        password = content['password']
        reader_type = content['reader_type'].lower()
        phone_no = content['phone_no']
        email_id = content['email_id']
        hint = content.get('hint')  # hint nullable by creating tables again
        membership_period = content['membership_period'].lower()
        # todo email validation for other domains - check vicks

        username_exists = ReaderRepo.get_reader_by_username(username)

        if username_exists:
            raise InvalidInputException(
                'username {} already exists. Please use a different username'.format(content['username']))

        reader_type = MembershipTypeEnum.get_membership_enum(reader_type).name
        if not reader_type:
            raise InvalidInputException('Please choose a valid Reader type platinum/gold/silver')

        if not phone_no.isdigit():
            raise InvalidInputException('Please enter a valid phone number without +country code')

        is_password_bad = is_bad_password(password)
        if is_password_bad:
            return is_password_bad

        if not email_validation(email_id):
            raise InvalidInputException('Please enter a valid email address')

        membership_period_enum = PeriodOfMembership.get_membership_period_enum(membership_period)
        if not membership_period_enum:
            raise InvalidInputException(
                'Please choose the period of your membership annual or half year or quarter or month')

        try:
            reader_type_id = ReaderRepo.get_readertypeid(reader_type)
        except Exception:
            raise InvalidInputException(
                '{} readertype does not exist. Please choose a valid Reader type platinum/gold/silver'.format(
                    reader_type))

        new_reader = ReaderRepo.add_reader(content, membership_period_enum, reader_type_id)
        return new_reader
