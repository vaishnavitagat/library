from repository.book_repo import BookRepo
from library.libs.custom_exceptions import DbFetchFailureException


class BookService:
    @staticmethod
    def get_book_list():
        """
        Service to fetch all book recods from the database and returns the list of book objects.

        Raises:
            DbFetchFailureException: something

        Returns:
            list - list of Book objects
        """
        try:
            books = BookRepo.get_all_books()
        except Exception as ex:
            print('Unexpected error occurred while fetching all books. Error - {}'.format(ex))
            raise DbFetchFailureException('Unable to fetch db records.')

        book_list = []
        for book in books:
            book_dict = book.convert_obj_to_dict(book, book.all_book_attributes)
            book_list.append(book_dict)

        return book_list

    @staticmethod
    def get_book_by_id(book_id):
        """
        Service to fetch book record by id from the database and returns the list of book objects.

        Raises:
            DbFetchFailureException: something

        Returns:
            list - list of Book objects
        """
        book = BookRepo.get_book_by_id(book_id)
        return book.convert_obj_to_dict(book, book.all_book_attributes)

    @staticmethod
    def get_book_by_category(category):
        """
               Service to fetch all book records by category from the database and returns the list of book objects.

               Raises:
                   DbFetchFailureException: something

               Returns:
                   list - list of Book objects
               """
        try:
            books = BookRepo.get_book_by_category(category)
        except Exception as ex:
            print('Unexpected error occurred while fetching all books. Error - {}'.format(ex))
            raise DbFetchFailureException('Unable to fetch db records.')

        book_list = []
        for book in books:
            book_dict = book.convert_obj_to_dict(book, book.all_book_attributes)
            book_list.append(book_dict)

        return book_list

    @staticmethod
    def get_book_by_author(author):
        try:
            books = BookRepo.get_book_by_author(author)
        except Exception as ex:
            print('Unexpected error occurred while fetching all books. Error - {}'.format(ex))
            raise DbFetchFailureException('Unable to fetch db records.')

        book_list = []
        for book in books:
            book_dict = book.convert_obj_to_dict(book, book.all_book_attributes)
            book_list.append(book_dict)

        return book_list


