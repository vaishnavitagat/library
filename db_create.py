"""
this is a script to create all the tables in the database
"""
from sqlalchemy import create_engine
from sqlalchemy.schema import CreateSchema

from models import Base
#it is mandatory to import all the tables here to create all tables
from models.account import Account
from models.book import Book
from models.issued_books import IssuedBooks
from models.librarian import Librarian
from models.notification import Notification
from models.passwordhistory import PasswordHistory
from models.prebook import Prebook
from models.rating import Rating
from models.reader import Reader
from models.readertype import ReaderType
from models.book import BookStock
from models.transaction import Transaction
from models.waiting_queue import WaitingQueue
from models.reserved_book import ReservedBook

endpoint = 'postgresql://postgres:hp12@localhost:5432/library_db'
engine = create_engine(endpoint, echo=True)
engine.execute(CreateSchema('application'))
Base.metadata.create_all(engine)
